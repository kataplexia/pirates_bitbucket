﻿using UnityEngine;

public interface ISpriteCollection
{
    Transform SpriteParent
    {
        get;
        set;
    }
        
    Sprite BodySprite
    {
        get;
        set;
    }

    Sprite LegsSprite
    {
        get;
        set;
    }

    Sprite HairSprite
    {
        get;
        set;
    }

    Sprite ArmorSprite
    {
        get;
        set;
    }

    Sprite WeaponSprite
    {
        get;
        set;
    }

    void GenerateSpriteCollection();
}