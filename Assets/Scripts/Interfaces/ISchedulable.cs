﻿public interface ISchedulable
{
    int Time
    {
        get;
    }
}