﻿using UnityEngine;

public enum ActorGenders
{
    NONE,
    Male,
    Female
}

public enum ActorZodiacs
{
    NONE,
    Aries,      // STR STR
    Leo,        // STR INT    
    Cancer,     // INT DEX
    Pisces,     // INT INT
    Scorpio,    // DEX INT
    Taurus,     // STR DEX
    Sagitarius, // DEX DEX
    Gemini,     // STR STR
    Virgo,      // INT INT
    Libra,      // DEX DEX
    Capricorn,  // DEX STR
    Aquarius    // INT STR
}

public enum ActorNatures
{
    NONE,
    Forceful,
    Cunning,
    Wise
}

public enum ActorQualities
{
    NONE,
    Common,
    Uncommon,
    Rare,
    Epic
}
    
public interface IActor
{
    ActorGenders ActorGender
    {
        get;
        set;
    }

    ActorZodiacs ActorZodiac
    {
        get;
        set;
    }

    Color ZodiacColor
    {
        get;
        set;
    }

    ActorNatures ActorNature
    {
        get;
        set;
    }

    ActorQualities ActorQuality
    {
        get;
        set;
    }

    Color QualityColor
    {
        get;
        set;
    }

    int AttackPowerValue
    {
        get;
        set;
    }

    int MagicPowerValue
    {
        get;
        set;
    }

    int CriticalChanceValue
    {
        get;
        set;
    }

    int HitChanceValue
    {
        get;
        set;
    }

    int PhysicalResistanceValue
    {
        get;
        set;
    }

    int MagicResistanceValue
    {
        get;
        set;
    }

    int DodgeChanceValue
    {
        get;
        set;
    }

    int CurrentHealthValue
    {
        get;
        set;
    }

    int MaxHealthValue
    {
        get;
        set;
    }

    int SpeedValue
    {
        get;
        set;
    }

    string Name
    {
        get;
        set;
    }
}