﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class TypeDamageModifier : MonoBehaviour
{
    public string path = "Assets/Resources/Test sheet.csv";
    public float[,] damageEffectiveness = new float[3,3];

    void Start()
    {
        ReadCSVFile();
    }

    public float DamageModifier(int type1, int type2)
    {
        float damageMod = damageEffectiveness[type1, type2];
        return damageMod;
    }

    private void ReadCSVFile()
    {
        string fileData = System.IO.File.ReadAllText(path);
        string[] line = fileData.Split('\n');

        for (int i = 0; i < line.Length; i++)//running through rows
        {
            string[] lineData = line[i].Split(',');

            for (int o = 0; o < lineData.Length; o++)//putting the data from lines(the rows) and lineData(the columns) into a 2d array
            {
                bool tryParse = float.TryParse(lineData[o], out damageEffectiveness[i, o]);

                if (!tryParse)//catching an error with a string/empty value in the spreadsheet
                {
                    Debug.Log("Error when trying to parse float");
                    break;
                }
                    
            }
        }
    }
}
