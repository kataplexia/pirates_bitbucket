﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Units : MonoBehaviour
{
    public int elementalType;
    public float 
        statAlign1, statAlign2, 
        health, maxHealth,
        offensiveStat, defensiveStat;

    private TypeDamageModifier damageMod;

    void Start()
    {
        damageMod = GameObject.Find("GameController").GetComponent<TypeDamageModifier>();
        offensiveStat = 5;//instead of 5 put some math shit for the stat alignment types
        maxHealth = 100;//instead of 100 put some math shit for the stat alignment types
        health = maxHealth;
    }

    public virtual void DamageCall(Units enemyUnit)
    {
        float damageToDeal = offensiveStat;

        damageToDeal *= damageMod.DamageModifier(elementalType, enemyUnit.elementalType);

        enemyUnit.TakeDamage(damageToDeal);//dealing damage to the enemy unit, might be a better way to do this.
    }

    private void TakeDamage(float damage)
    {
        health -= damage;
    }
}
