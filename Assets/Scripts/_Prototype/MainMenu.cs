﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private Data data;

    public bool savedGamePresent;
    public Button continueButton;

    private void Awake()
    {
        data = GameObject.Find("_DATA").GetComponent<Data>();
    }

    private void Start()
    {
        if (!savedGamePresent)
            continueButton.interactable = false;
    }

    public void NewGame()
    {
        // clear saved data here
        data.currentGold = 450;
        data.currentMorale = 100;
        SceneManager.LoadScene("_SHIP", LoadSceneMode.Single);
    }

    public void Quit()
    {
        // save any game data here
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else 
        Application.Quit();
        #endif
    }
}
