﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit
{
    public enum UnitClasses
    {
        Warrior,
        Rogue,
        Mage
    }

    public UnitClasses unitClass;
    public string unitName;
    public int unitCost;

    private UnitNameGenerator unitNameGenerator = new UnitNameGenerator();
    public Unit()
    {
        int randomClass = Random.Range(0, 3);
        if (randomClass == 0)
        {
            unitClass = UnitClasses.Warrior;
        }
        else if (randomClass == 1)
        {
            unitClass = UnitClasses.Rogue;
        }
        else if (randomClass == 2)
        {
            unitClass = UnitClasses.Mage;
        }
        unitName = unitNameGenerator.GeneratedName();
        unitCost = 150;
    }
}
