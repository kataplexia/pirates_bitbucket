﻿using System.Collections.Generic;
using UnityEngine;

public class CellGenerator : MonoBehaviour
{
    public enum GenerationModes
    {
        TOPDOWN,
        ISO
    }
    public GenerationModes generationMode;

    public GameObject cellPrefab;
    public GameObject cellPrefabISO;

    public float cellSize;
    public int rowCount;
    public int columnCount;

    private Vector2 startingPosition;

    public void GenerateCells(List<List<Cell>> cellCollection)
    {
        if (generationMode == GenerationModes.TOPDOWN)
        {
            TopDownGeneration(cellCollection);
        }
        else
        {
            ISOGeneration(cellCollection);
        }
    }

    private void TopDownGeneration(List<List<Cell>> cellCollection)
    {
        float startingPosX = ((columnCount * cellSize) / -2) + (cellSize / 2);
        float startingPosY = ((rowCount * cellSize) / -2) + (cellSize / 2);
        startingPosition = new Vector2(startingPosX, startingPosY);

        for (int r = 0; r < columnCount; r++)
        {
            List<Cell> row = new List<Cell>();
            for (int c = 0; c < rowCount; c++)
            {
                GameObject newCell = Instantiate(cellPrefab, this.transform);

                float posX = startingPosition.x + c * cellSize;
                float posY = startingPosition.y + r * cellSize;
                float posZ = newCell.transform.position.z;

                newCell.transform.position = new Vector3(posX, posY, posZ);

                Cell cellComponent = newCell.GetComponent<Cell>();
                cellComponent.X = c;
                cellComponent.Y = r;
                newCell.transform.name = string.Format("Cell[{0}, {1}]", c, r);

                row.Add(cellComponent);
            }
            cellCollection.Add(row);
        }
    }

    private void ISOGeneration(List<List<Cell>> cellCollection)
    {
        float startingPosX = (rowCount * (cellSize / -4)) + (columnCount * (cellSize / -4)) + (cellSize / 2);
        float startingPosY = (rowCount * (cellSize / -8)) + (columnCount * (cellSize / 8));

        startingPosition = new Vector2(startingPosX, startingPosY);

        for (int y = 0; y < rowCount; y++)
        {
            List<Cell> row = new List<Cell>();
            for (int x = 0; x < columnCount; x++)
            {
                GameObject newCell = Instantiate(cellPrefabISO, this.transform);

                float posX = (startingPosition.x + x * cellSize / 2) + (y * cellSize / 2);
                float posY = startingPosition.y + y * (cellSize / 4) - (x * cellSize / 4);
                float posZ = newCell.transform.position.z;

                newCell.transform.position = new Vector3(posX, posY, posZ);

                Cell cellComponent = newCell.GetComponent<Cell>();
                cellComponent.X = x;
                cellComponent.Y = y;
                newCell.transform.name = string.Format("Cell[{0}, {1}]", x, y);

                row.Add(cellComponent);
            }
            cellCollection.Add(row);
        }
    }
}
