﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : MonoBehaviour
{
    public CellGenerator cellGenerator;
    public List<List<Cell>> cellCollection = new List<List<Cell>> ();

    void Start()
    {
        cellGenerator.GenerateCells(cellCollection);
    }

    public Cell GetCellAt(int X, int Y)
    {
        return cellCollection[X][Y];
    }

    public void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

        if (hit.collider == null)
            return;

        if (hit.collider.gameObject.layer == 8)
        {
            Cell cell = hit.collider.transform.parent.GetComponent<Cell>();
            cell.Highlight();
        }
    }
}
