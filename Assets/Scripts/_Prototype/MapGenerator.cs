﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

using UnityEngine.UI;

public class TextureCollection
{
    public Texture monster;
    public Texture elite;
    public Texture unknown;
    public Texture shop;
    public Texture chest;
    public Texture boss;

    public void Initialize()
    {
        monster = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Textures/monster.png", typeof(Texture));
        elite = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Textures/elite.png", typeof(Texture));
        unknown = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Textures/unknown.png", typeof(Texture));
        shop = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Textures/shop.png", typeof(Texture));
        chest = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Textures/chest.png", typeof(Texture));
        boss = (Texture)AssetDatabase.LoadAssetAtPath("Assets/Textures/boss.png", typeof(Texture));
    }
}

public class Node
{
    public int type;
    public Node next1;
    public Node next2;
    public int level;
    public bool beConnected;
    public bool toConnect;
    public int id;
    public Connection connection = new Connection();
}

public class Connection
{
    public int index;
    public int connections;
}

public class Level
{
    public List<Node> nodes = new List<Node>();
}

public class LevelsCollection
{
    public List<Level> levels = new List<Level>();
}

public class Drawable
{
    public int id;
    public Vector2 position;
    public Color color;
    public float size;
    public int type;
    public int next1;
    public int next2;
}

public class DrawablesCollection
{
    public List<Drawable> drawables = new List<Drawable>();
}

public class MapGenerator : MonoBehaviour
{
    private void Start()
    {
        LevelsCollection levelsCollection = new LevelsCollection();

        ConstructMap(16, levelsCollection);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            drawing = false;
            Start();
        }
    }

    private void ConstructMap(int length, LevelsCollection levelsCollection)
    {
        CreateNodes(length, levelsCollection);
        BuildPaths(length, levelsCollection);
        Shake(levelsCollection);
        Draw(levelsCollection);
    }

    private void CreateNodes(int length, LevelsCollection levelsCollection)
    {
        // Create nodes within the levels
        int id = 1;

        for (int i = 0; i < length; i++)
        {
            Level level = new Level();

            for (int j = 0; j < 5; j++)
            {
                Node node = new Node();
                node.type = AssignNodeType();
                node.next1 = null;
                node.next2 = null;
                node.level = i + 1;
                node.beConnected = false;
                node.id = id++;
                level.nodes.Add(node);
            }

            levelsCollection.levels.Add(level);
        }

        // Set nodes on the first level to 'beConnected'
        for (int i = 0; i < levelsCollection.levels[0].nodes.Count; i++)
        {
            levelsCollection.levels[0].nodes[i].beConnected = true;
        }

        // Create and add the final node and level
        Level finalLevel = new Level();
        Node finalNode = new Node();

        finalNode.type = 5;
        finalNode.level = length;
        finalNode.beConnected = true;
        finalNode.toConnect = true;
        finalNode.id = 666;

        finalLevel.nodes.Add(finalNode);
        levelsCollection.levels.Add(finalLevel);
    }

    private void BuildPaths(int length, LevelsCollection levelsCollection)
    {
        for (int i = 0; i < length - 2; i++)
        {
            Connection connection = new Connection();

            for (int j = 0; j < levelsCollection.levels[i].nodes.Count; j++)
            {
                if (!levelsCollection.levels[i].nodes[j].beConnected && i != 0)
                {
                    break;
                }

                if ((Random.Range(0f, 1.0f) <= 0.1f || connection.connections == 2) && j != 0)
                {
                    connection.index++;
                    connection.connections = 0;
                }

                if (Random.Range(0f, 1.0f) <= 0.45f)
                {
                    levelsCollection.levels[i].nodes[j].next1 = levelsCollection.levels[i + 1].nodes[connection.index];

                    if (levelsCollection.levels[i + 1].nodes[connection.index] != null)
                    {
                        levelsCollection.levels[i + 1].nodes[connection.index].beConnected = true;
                    }

                    connection.index++;
                    if (connection.index > 4)
                    {
                        break;
                    }

                    levelsCollection.levels[i].nodes[j].next2 = levelsCollection.levels[i + 1].nodes[connection.index];
                    if (connection.index > 4)
                    {
                        break;
                    }

                    levelsCollection.levels[i + 1].nodes[connection.index].beConnected = true;
                    connection.connections = 1;
                }
                else
                {
                    levelsCollection.levels[i].nodes[j].next1 = levelsCollection.levels[i + 1].nodes[connection.index];

                    connection.connections++;
                    if (connection.index > 4)
                    {
                        break;
                    }

                    levelsCollection.levels[i + 1].nodes[connection.index].beConnected = true;
                }

                if (levelsCollection.levels[i].nodes[j].next1 != null)
                {
                    levelsCollection.levels[i].nodes[j].toConnect = true;
                }
            }
        }

        for (int i = 0; i < levelsCollection.levels[levelsCollection.levels.Count - 2].nodes.Count; i++)
        {
            if (levelsCollection.levels[levelsCollection.levels.Count - 2].nodes[i].beConnected)
            {
                levelsCollection.levels[levelsCollection.levels.Count - 2].nodes[i].next1
                    = levelsCollection.levels[levelsCollection.levels.Count - 1].nodes[0];
                levelsCollection.levels[levelsCollection.levels.Count - 2].nodes[i].toConnect = true;
            }
        }
    }

    private void Shake(LevelsCollection levelsCollection)
    {
        for (int i = levelsCollection.levels.Count - 1; i >= 0 ; i--)
        {
            for (int j = levelsCollection.levels[i].nodes.Count - 1; j >= 0 ; j--)
            {
                if (!levelsCollection.levels[i].nodes[j].beConnected ||
                    !levelsCollection.levels[i].nodes[j].toConnect)
                {
                    levelsCollection.levels[i].nodes.RemoveAt(j);
                }
            }
        }
    }

    public GameObject buttonPrefab;
    public Transform canvasTransform;
    private void Draw(LevelsCollection levelsCollection)
    {
        drawablesCollection = new DrawablesCollection();

        for (int i = 0; i < levelsCollection.levels.Count; i++)
        {
            for (int j = 0; j < levelsCollection.levels[i].nodes.Count; j++)
            {
                Drawable drawable = new Drawable();
                drawable.id = levelsCollection.levels[i].nodes[j].id;

                float buttonXPos = j * 100 - 200;
                float buttonYPos = i * 25 - 200;

                GameObject buttonObj = Instantiate(buttonPrefab, canvasTransform);
                buttonObj.GetComponent<RectTransform>().localPosition = new Vector3(buttonXPos, buttonYPos, 0f);

                Vector2 position = new Vector2(buttonXPos, buttonYPos);
                drawable.position = position;

                drawable.color = Color.red;
                drawable.size = 40f;
                drawable.type = levelsCollection.levels[i].nodes[j].type;

                if (levelsCollection.levels[i].nodes[j].next1 != null)
                {
                    drawable.next1 = levelsCollection.levels[i].nodes[j].next1.id;
                }

                if (levelsCollection.levels[i].nodes[j].next2 != null)
                {
                    drawable.next2 = levelsCollection.levels[i].nodes[j].next2.id;
                }

                drawablesCollection.drawables.Add(drawable);
            }
        }

        /// THIS SNIPPET IS FOR MAKING LINES
        /*
        //PointA and PointB determined before this
        Vector3 midpoint = (pointA + pointB) / 2; //used to position line
        float pointDistance = (pointA - pointB).magnitude; //used for height of line

        //really need to figure out what all this does one of these days. Not even my first game using it...
        float angle = Mathf.Atan2(pointB.x - pointA.x, pointA.y - pointB.y);
        if (angle < 0.0) { angle += Mathf.PI * 2; }
        angle *= Mathf.Rad2Deg;

        float lineWidth = .01f;
        lineRect.anchorMin = lineRect.anchorMax = midpoint; //move to midpoint, then expand to the correct size around it
        lineRect.anchorMin -= new Vector2(lineWidth / 2, pointDistance / 2);
        lineRect.anchorMax += new Vector2(lineWidth / 2, pointDistance / 2);
        lineRect.rotation = Quaternion.Euler(0, 0, angle); //rotate around the mid point
        */

        /*
        drawablesCollection = new DrawablesCollection();

        for (int i = 0; i < levelsCollection.levels.Count; i++)
        {
            for (int j = 0; j < levelsCollection.levels[i].nodes.Count; j++)
            {
                Drawable drawable = new Drawable();
                drawable.id = levelsCollection.levels[i].nodes[j].id;

                float xPos = j * 120 + 30 - 60 * Random.Range(0.1f, 1.0f);
                float yPos = i * 140 + 30 + 35 - 70 * Random.Range(0.1f, 1.0f);

                float buttonXPos = j * 100 - 200;
                float buttonYPos = i * 25 - 200;

                GameObject buttonObj = Instantiate(buttonPrefab, canvasTransform);
                buttonObj.GetComponent<RectTransform>().localPosition = new Vector3(buttonXPos, buttonYPos, 0f);

                Vector2 position = new Vector2(xPos, yPos);
                drawable.position = position;

                drawable.color = Color.red;
                drawable.size = 40f;
                drawable.type = levelsCollection.levels[i].nodes[j].type;
                */

        /*
        if (i == levelsCollection.levels.Count - 2)
        {
            drawable.next1 =
                levelsCollection.levels[levelsCollection.levels.Count - 1].nodes[0].id;
            drawable.next2 =
                levelsCollection.levels[levelsCollection.levels.Count - 1].nodes[0].id;
        }
        */

        /*
        if (levelsCollection.levels[i].nodes[j].next1 != null)
        {
            drawable.next1 = levelsCollection.levels[i].nodes[j].next1.id;
        }

        if (levelsCollection.levels[i].nodes[j].next2 != null)
        {
            drawable.next2 = levelsCollection.levels[i].nodes[j].next2.id;
        }

        drawablesCollection.drawables.Add(drawable);
    }
}

textureCollection = new TextureCollection();
textureCollection.Initialize();
drawing = true;
*/
    }

    private TextureCollection textureCollection;
    private DrawablesCollection drawablesCollection;
    private bool drawing = false;
    private void OnDrawGizmos()
    {
        if (!drawing)
            return;

        foreach (Drawable drawable in drawablesCollection.drawables)
        {
            Gizmos.color = drawable.color;

            // Texture drawTexture = null;
            switch (drawable.type)
            {
                case 0:
                    // drawTexture = textureCollection.monster;
                    Gizmos.color = Color.red;
                    break;

                case 1:
                    // drawTexture = textureCollection.elite;
                    Gizmos.color = Color.magenta;
                    break;

                case 2:
                    // drawTexture = textureCollection.unknown;
                    Gizmos.color = Color.blue;
                    break;

                case 3:
                    // drawTexture = textureCollection.shop;
                    Gizmos.color = Color.yellow;
                    break;

                case 4:
                    // drawTexture = textureCollection.chest;
                    Gizmos.color = Color.cyan;
                    break;

                case 5:
                    // drawTexture = textureCollection.boss;
                    Gizmos.color = Color.green;
                    break;
            }

            // Gizmos.DrawGUITexture(new Rect(Vector2.zero, new Vector2(Screen.width, Screen.height)), drawTexture);

            Gizmos.DrawSphere(drawable.position, drawable.size);
        }

        for (int i = 0; i < drawablesCollection.drawables.Count; i++)
        {
            for (int j = i; j < drawablesCollection.drawables.Count; j++)
            {
                if (drawablesCollection.drawables[i].next1 == drawablesCollection.drawables[j].id ||
                    drawablesCollection.drawables[i].next2 == drawablesCollection.drawables[j].id)
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine(drawablesCollection.drawables[i].position, drawablesCollection.drawables[j].position);
                }
            }
        }
    }

    private int AssignNodeType()
    {
        return Random.Range(0f, 1.0f) < 0.4f ? 0 
            : (Random.Range(0f, 1.0f) < 0.5f ? 1 
            : (Random.Range(0f, 1.0f) < 0.5f ? 2 
            : (Random.Range(0f, 1.0f) < 0.5f ? 3 : 4)));
    }
}


