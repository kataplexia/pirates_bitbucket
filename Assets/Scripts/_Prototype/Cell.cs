﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public int X;
    public int Y;

    public SpriteRenderer highlightSprite;

    public void Highlight()
    {
        highlightSprite.enabled = true;
    }
}
