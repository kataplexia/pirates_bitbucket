﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ShipMenu : MonoBehaviour
{
    private Data data;

    public CanvasGroup shipMenuCanvasGroup;
    public CanvasGroup recruitMenuCanvasGroup;
    public CanvasGroup unitStatisticsCanvasGroup;

    public Text goldText;
    public Text moraleText;

    private List<PlayerActor> recruitableActors;

    private void Awake()
    {
        data = GameObject.Find("_DATA").GetComponent<Data>();
        recruitableActors = new List<PlayerActor>();
    }

    private void Start()
    {
        UpdateResources();
        PopulateRecruit();
    }

    private void PopulateRecruit()
    {
        for (int i = 0; i < 4; i++)
        {
            PlayerActor playerActor = new PlayerActor();
            recruitableActors.Add(playerActor);
        }
        UpdateRecruitMenu();
    }

    public void Onward()
    {
        if (data.PlayerActors.Count == 0)
        {
            data.DisplayDebugText("You have no crewmembers to depart with.");
            return;
        }

        SceneManager.LoadScene("_COMBAT", LoadSceneMode.Single);
    }

    public void Recruit(int index)
    {
        if (data.PlayerActors.Count >= 4)
        {
            data.DisplayDebugText("Team at maximum size. Cannot recruit.");
            return;
        }

        int cost = recruitableActors[index].ActorCost;
        if (data.currentGold < cost)
        {
            data.DisplayDebugText("Not enough gold to recruit.");
            return;
        }

        data.currentGold -= cost;

        data.PlayerActors.Add(recruitableActors[index]);
        recruitableActors.RemoveAt(index);

        UpdateRecruitMenu();
        UpdateCrewMenu();
        UpdateResources();
    }

    private void UpdateRecruitMenu()
    {
        for (int i = 0; i < 4; i++)
        {
            Transform unitPanel = GameObject.Find("RecruitPanel_" + (i + 1).ToString()).transform;
            PlayerActor playerActor = null;
            if (i < recruitableActors.Count)
            {
                playerActor = recruitableActors[i];
            }

            Text unitNameText = unitPanel.GetChild(0).GetComponent<Text>();
            Text unitClassText = unitPanel.GetChild(1).GetComponent<Text>();
            Text unitCostText = unitPanel.GetChild(2).GetComponent<Text>();
            Transform recruitButton = unitPanel.GetChild(3);
            Image spritePanel = unitPanel.GetChild(4).GetComponent<Image>();

            if (playerActor != null)
            {
                string genderSymbol = "";
                if (playerActor.ActorGender == ActorGenders.Male)
                {
                    genderSymbol = "♂";
                }
                else
                {
                    genderSymbol = "♀";
                }

                unitNameText.text = genderSymbol + " " + playerActor.Name;
                unitNameText.color = playerActor.QualityColor;
                unitNameText.enabled = true;

                unitClassText.text = playerActor.ActorZodiac.ToString();
                unitClassText.color = playerActor.ZodiacColor;
                unitClassText.enabled = true;

                unitCostText.text = playerActor.ActorCost.ToString();
                unitCostText.enabled = true;

                recruitButton.GetComponent<Image>().enabled = true;
                recruitButton.GetComponent<Button>().enabled = true;
                recruitButton.GetChild(0).GetComponent<Text>().enabled = true;

                spritePanel.enabled = true;
                Image bodySprite = spritePanel.transform.GetChild(0).GetComponent<Image>();
                bodySprite.sprite = playerActor.BodySprite;
                Image legsSprite = spritePanel.transform.GetChild(1).GetComponent<Image>();
                legsSprite.sprite = playerActor.LegsSprite;
                Image hairSprite = spritePanel.transform.GetChild(2).GetComponent<Image>();
                hairSprite.sprite = playerActor.HairSprite;
                bodySprite.enabled = true;
                legsSprite.enabled = true;
                hairSprite.enabled = true;
            }
            else
            {
                unitNameText.enabled = false;

                unitClassText.enabled = false;

                unitCostText.enabled = false;

                recruitButton.GetComponent<Image>().enabled = false;
                recruitButton.GetComponent<Button>().enabled = false;
                recruitButton.GetChild(0).GetComponent<Text>().enabled = false;

                spritePanel.enabled = false;
                Image bodySprite = spritePanel.transform.GetChild(0).GetComponent<Image>();
                Image legsSprite = spritePanel.transform.GetChild(1).GetComponent<Image>();
                Image hairSprite = spritePanel.transform.GetChild(2).GetComponent<Image>();
                bodySprite.enabled = false;
                legsSprite.enabled = false;
                hairSprite.enabled = false;
            }
            
        } 
    }

    private void UpdateCrewMenu()
    {
        for (int i = 0; i < 4; i++)
        {
            Transform crewPanel = GameObject.Find("CrewPanel_" + (i + 1).ToString()).transform;
            PlayerActor playerActor = null;
            if (i < data.PlayerActors.Count)
            {
                playerActor = data.PlayerActors[i];
            }

            Text unitNameText = crewPanel.GetChild(0).GetComponent<Text>();
            Text unitClassText = crewPanel.GetChild(1).GetComponent<Text>();
            Image spritePanel = crewPanel.GetChild(2).GetComponent<Image>();

            if (playerActor != null)
            {
                string genderSymbol = "";
                if (playerActor.ActorGender == ActorGenders.Male)
                {
                    genderSymbol = "♂";
                }
                else
                {
                    genderSymbol = "♀";
                }

                unitNameText.text = genderSymbol + " " + playerActor.Name;
                unitNameText.color = playerActor.QualityColor;
                unitNameText.enabled = true;

                unitClassText.text = playerActor.ActorZodiac.ToString();
                unitClassText.color = playerActor.ZodiacColor;
                unitClassText.enabled = true;

                spritePanel.enabled = true;
                Image bodySprite = spritePanel.transform.GetChild(0).GetComponent<Image>();
                bodySprite.sprite = playerActor.BodySprite;
                Image legsSprite = spritePanel.transform.GetChild(1).GetComponent<Image>();
                legsSprite.sprite = playerActor.LegsSprite;
                Image hairSprite = spritePanel.transform.GetChild(2).GetComponent<Image>();
                hairSprite.sprite = playerActor.HairSprite;
                bodySprite.enabled = true;
                legsSprite.enabled = true;
                hairSprite.enabled = true;
            }
            else
            {
                unitNameText.enabled = false;
                unitClassText.enabled = false;

                spritePanel.enabled = false;
                Image bodySprite = spritePanel.transform.GetChild(0).GetComponent<Image>();
                Image legsSprite = spritePanel.transform.GetChild(1).GetComponent<Image>();
                Image hairSprite = spritePanel.transform.GetChild(2).GetComponent<Image>();
                bodySprite.enabled = false;
                legsSprite.enabled = false;
                hairSprite.enabled = false;
            }

        }
    }

    public void ShowRecruitableUnitStatistics (int actorIndex)
    {
        PlayerActor playerActor = null;
        if (actorIndex < recruitableActors.Count)
        {
            playerActor = recruitableActors[actorIndex];
        }

        if (unitStatisticsCanvasGroup.alpha == 0 && playerActor != null)
        {
            SetupUnitStatisticsPanel(playerActor);
            unitStatisticsCanvasGroup.alpha = 1;
        }
    }

    public void ShowCrewUnitStatistics(int actorIndex)
    {
        PlayerActor playerActor = null;
        if (actorIndex < data.PlayerActors.Count)
        {
            playerActor = data.PlayerActors[actorIndex];
        }

        if (unitStatisticsCanvasGroup.alpha == 0 && playerActor != null)
        {
            SetupUnitStatisticsPanel(playerActor);
            unitStatisticsCanvasGroup.alpha = 1;
        }
    }

    private void SetupUnitStatisticsPanel(PlayerActor playerActor)
    {
        Transform statsPanel = unitStatisticsCanvasGroup.transform;

        Text actorNameText = statsPanel.GetChild(0).GetComponent<Text>();
        Text actorZodiacText = statsPanel.GetChild(1).GetComponent<Text>();
        Text actorNatureText = statsPanel.GetChild(2).GetComponent<Text>();
        Text actorQualityText = statsPanel.GetChild(3).GetComponent<Text>();
        Image spritePanel = statsPanel.GetChild(4).GetComponent<Image>();

        Text actorAttackPowerText = statsPanel.GetChild(5).GetComponent<Text>();
        Text actorMagicPowerText = statsPanel.GetChild(6).GetComponent<Text>();
        Text actorCriticalChanceText = statsPanel.GetChild(7).GetComponent<Text>();
        Text actorHitChanceText = statsPanel.GetChild(8).GetComponent<Text>();
        Text actorPhysicalResistText = statsPanel.GetChild(9).GetComponent<Text>();
        Text actorMagicalResistText = statsPanel.GetChild(10).GetComponent<Text>();
        Text actorDodgeChanceText = statsPanel.GetChild(11).GetComponent<Text>();
        Text actorHealthText = statsPanel.GetChild(12).GetComponent<Text>();
        Text actorSpeedText = statsPanel.GetChild(13).GetComponent<Text>();

        string genderSymbol = "";
        if (playerActor.ActorGender == ActorGenders.Male)
        {
            genderSymbol = "♂";
        }
        else
        {
            genderSymbol = "♀";
        }

        actorNameText.text = genderSymbol + " " + playerActor.Name;
        actorNameText.color = playerActor.QualityColor;

        actorZodiacText.text = "Zodiac Sign: " + playerActor.ActorZodiac.ToString();
        actorZodiacText.color = playerActor.ZodiacColor;

        actorNatureText.text = "Nature: " + playerActor.ActorNature.ToString();
        switch (playerActor.ActorNature)
        {
            case ActorNatures.Forceful:
                actorNatureText.color = Color.red;
                break;

            case ActorNatures.Cunning:
                actorNatureText.color = Color.green;
                break;

            case ActorNatures.Wise:
                actorNatureText.color = Color.blue;
                break;
        }

        actorQualityText.text = "Quality: " + playerActor.ActorQuality.ToString();
        actorQualityText.color = playerActor.QualityColor;

        spritePanel.enabled = true;
        Image bodySprite = spritePanel.transform.GetChild(0).GetComponent<Image>();
        bodySprite.sprite = playerActor.BodySprite;
        Image legsSprite = spritePanel.transform.GetChild(1).GetComponent<Image>();
        legsSprite.sprite = playerActor.LegsSprite;
        Image hairSprite = spritePanel.transform.GetChild(2).GetComponent<Image>();
        hairSprite.sprite = playerActor.HairSprite;

        actorAttackPowerText.text = "Attack Power: " + playerActor.AttackPowerValue.ToString();
        actorMagicPowerText.text = "Magic Power: " + playerActor.MagicPowerValue.ToString();
        actorCriticalChanceText.text = "Critical Chance: " + playerActor.CriticalChanceValue.ToString() + "%";
        actorHitChanceText.text = "Hit Chance: " + playerActor.HitChanceValue.ToString() + "%";
        actorPhysicalResistText.text = "Physical Resistance: " + playerActor.PhysicalResistanceValue.ToString();
        actorMagicalResistText.text = "Magical Resistance: " + playerActor.MagicResistanceValue.ToString();
        actorDodgeChanceText.text = "Dodge Chance: " + playerActor.DodgeChanceValue.ToString() + "%";
        actorHealthText.text = "Health: " + playerActor.CurrentHealthValue.ToString() + "/" + playerActor.MaxHealthValue.ToString();
        actorSpeedText.text = "Speed: " + playerActor.SpeedValue.ToString();
    }

    public void HideUnitStatistics()
    {
        if (unitStatisticsCanvasGroup.alpha == 1)
        {
            unitStatisticsCanvasGroup.alpha = 0;
        } 
    }

    private void UpdateResources()
    {
        goldText.text = "G: " + data.currentGold.ToString();
        moraleText.text = "M: " + data.currentMorale.ToString() + "%";
    }

    private void HideCanvasGroup(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 0;
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
    }

    private void ShowCanvasGroup(CanvasGroup canvasGroup)
    {
        canvasGroup.alpha = 1;
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
    }

    public void ShowRecruit()
    {
        HideCanvasGroup(shipMenuCanvasGroup);
        ShowCanvasGroup(recruitMenuCanvasGroup);
    }

    public void HideRecruit()
    {
        HideCanvasGroup(recruitMenuCanvasGroup);
        ShowCanvasGroup(shipMenuCanvasGroup);
    }
}
