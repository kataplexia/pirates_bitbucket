﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    public int currentGold;
    public int currentMorale;

    public MeshRenderer debugTextRenderer;
    public TextMesh debugTextMesh;

    public SpriteData SpriteData;
    public ActorNameGenerator ActorNameGenerator;
    public List<PlayerActor> PlayerActors = new List<PlayerActor>();

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        SpriteData = this.GetComponent<SpriteData>();
        ActorNameGenerator = new ActorNameGenerator();
    }

    public void DisplayDebugText(string text)
    {
        Debug.LogWarning(text);
        debugTextMesh.text = text;
        debugTextRenderer.enabled = true;
        StopCoroutine("HideDebugText");
        StartCoroutine("HideDebugText");
    }

    private IEnumerator HideDebugText()
    {
        yield return new WaitForSeconds(2f);
        debugTextRenderer.enabled = false;
    }
}
