﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapGenerator : MonoBehaviour
{
    public Grid grid;
    public Tilemap tileMap;
    public TileBase tileBase;
    public TileBase tileBase_2;
    // public Vector3Int tilePos;
    public RectInt tileMapRect;

    void Start()
    {
        Vector3Int fillStart = new Vector3Int(tileMapRect.xMin, tileMapRect.yMin, 0);
        Vector3Int fillEnd = new Vector3Int(tileMapRect.xMax, tileMapRect.yMax, 0);
        BoxFill(tileMap, tileBase, fillStart, fillEnd);
    }

    public void BoxFill(Tilemap map, TileBase tile, Vector3Int start, Vector3Int end)
    {
        //Determine directions on X and Y axis
        var xDir = start.x < end.x ? 1 : -1;
        var yDir = start.y < end.y ? 1 : -1;
        //How many tiles on each axis?
        int xCols = 1 + Mathf.Abs(start.x - end.x);
        int yCols = 1 + Mathf.Abs(start.y - end.y);
        //Start painting
        for (var x = 0; x < xCols; x++)
        {
            for (var y = 0; y < yCols; y++)
            {
                var tilePos = start + new Vector3Int(x * xDir, y * yDir, 0);
                map.SetTile(tilePos, tile);
            }
        }
    }

    public void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D hit = Physics2D.GetRayIntersection(ray, Mathf.Infinity);

        if (hit.collider == null)
            return;

        if (hit.collider.gameObject.layer == 8)
        {
            Vector3 screenToWorld = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Debug.Log(screenToWorld);

            Vector3Int coordinate = grid.WorldToCell(screenToWorld);

            Debug.Log(coordinate);

            coordinate = new Vector3Int(coordinate.x, coordinate.y, 0);
            tileMap.SetTile(coordinate, tileBase_2);
        }
    }
}
