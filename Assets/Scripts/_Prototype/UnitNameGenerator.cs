﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;

public class UnitNameGenerator
{
    private string[] namesArray;

    public UnitNameGenerator()
    {
        var xmlRawFile = Resources.Load<TextAsset>("UnitNames");
        string xmlData = xmlRawFile.text;
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(new StringReader(xmlData));

        XmlNode nodeNames = xmlDocument.SelectSingleNode("/data/names");
        string[] seperatingChars = { ", " };

        string namesString = nodeNames.InnerXml;
        namesArray = namesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);
    }

    public string GeneratedName()
    {
        int nameIndexMax = namesArray.Length;
        int randomNameIndex = Random.Range(0, nameIndexMax);

        return namesArray[randomNameIndex];
    }
}
