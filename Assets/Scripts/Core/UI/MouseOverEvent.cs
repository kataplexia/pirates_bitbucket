﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseOverEvent : MonoBehaviour
{
    public UnityEvent mouseOverTrueEvent;
    public UnityEvent mouseOverFalseEvent;

    public void InvokeMouseOverTrueEvent()
    {
        mouseOverTrueEvent.Invoke();
    }

    public void InvokeMouseOverFalseEvent()
    {
        mouseOverFalseEvent.Invoke();
    }
}
