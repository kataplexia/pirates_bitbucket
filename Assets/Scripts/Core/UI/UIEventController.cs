﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIEventController : MonoBehaviour
{
    private GameObject currentMouseOverObject;
    private GameObject lastMouseOverObject;

    private void Update()
    {
        currentMouseOverObject = CheckMouseOverEvent();

        if (currentMouseOverObject != null)
        {
            lastMouseOverObject = currentMouseOverObject;
            currentMouseOverObject.GetComponent<MouseOverEvent>().InvokeMouseOverTrueEvent();
        }
        else if (lastMouseOverObject != null)
        {
            lastMouseOverObject.GetComponent<MouseOverEvent>().InvokeMouseOverFalseEvent();
            lastMouseOverObject = null;
        }
    }

    private GameObject CheckMouseOverEvent()
    {
        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = Input.mousePosition;

        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, raycastResults);

        for (int i = 0; i < raycastResults.Count; i++)
        {
            if (raycastResults[i].gameObject.GetComponent<MouseOverEvent>() != null)
            {
                return raycastResults[i].gameObject;
            }
        }
        return null;
    }
}
