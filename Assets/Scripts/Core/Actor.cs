﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Actor : IActor, ISpriteCollection, ISchedulable
{
    private ActorGenders actorGender;
    private ActorZodiacs actorZodiac;
    private Color zodiacColor;
    private ActorNatures actorNature;
    private ActorQualities actorQuality;
    private Color qualityColor;

    private int attackPowerValue;
    private int magicPowerValue;
    private int criticalChanceValue;
    private int hitChanceValue;
    private int physicalResistanceValue;
    private int magicResistanceValue;
    private int dodgeChanceValue;
    private int currentHealthValue;
    private int maxHealthValue;
    private int speedValue;
    private string name;

    private Transform spriteParent;
    private Sprite bodySprite;
    private Sprite legsSprite;
    private Sprite hairSprite;
    private Sprite armorSprite;
    private Sprite weaponSprite;

    public ActorZodiacs ActorZodiac
    {
        get { return actorZodiac; }
        set { actorZodiac = value; }
    }

    public Color ZodiacColor
    {
        get { return zodiacColor; }
        set { zodiacColor = value; }
    }

    public ActorGenders ActorGender
    {
        get { return actorGender; }
        set { actorGender = value; }
    }

    public ActorNatures ActorNature
    {
        get { return actorNature; }
        set { actorNature = value; }
    }

    public ActorQualities ActorQuality
    {
        get { return actorQuality; }
        set { actorQuality = value; }
    }

    public Color QualityColor
    {
        get { return qualityColor; }
        set { qualityColor = value; }
    }

    public int AttackPowerValue
    {
        get { return attackPowerValue; }
        set { attackPowerValue = value; }
    }

    public int MagicPowerValue
    {
        get { return magicPowerValue; }
        set { magicPowerValue = value; }
    }

    public int CriticalChanceValue
    {
        get { return criticalChanceValue; }
        set { criticalChanceValue = value; }
    }

    public int HitChanceValue
    {
        get { return hitChanceValue; }
        set { hitChanceValue = value; }
    }

    public int PhysicalResistanceValue
    {
        get { return physicalResistanceValue; }
        set { physicalResistanceValue = value; }
    }

    public int MagicResistanceValue
    {
        get { return magicResistanceValue; }
        set { magicResistanceValue = value; }
    }

    public int DodgeChanceValue
    {
        get { return dodgeChanceValue; }
        set { dodgeChanceValue = value; }
    }

    public int CurrentHealthValue
    {
        get { return currentHealthValue; }
        set { currentHealthValue = value; }
    }

    public int MaxHealthValue
    {
        get { return maxHealthValue; }
        set { maxHealthValue = value; }
    }

    public int SpeedValue
    {
        get { return speedValue; }
        set { speedValue = value; }
    }

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public int Time
    {
        get { return SpeedValue; }
    }

    public Transform SpriteParent
    {
        get { return spriteParent; }
        set { spriteParent = value; }
    }

    public Sprite BodySprite
    {
        get { return bodySprite; }
        set { bodySprite = value; }
    }

    public Sprite LegsSprite
    {
        get { return legsSprite; }
        set { legsSprite = value; }
    }

    public Sprite HairSprite
    {
        get { return hairSprite; }
        set { hairSprite = value; }
    }

    public Sprite ArmorSprite
    {
        get { return armorSprite; }
        set { armorSprite = value; }
    }

    public Sprite WeaponSprite
    {
        get { return weaponSprite; }
        set { weaponSprite = value; }
    }

    public abstract void GenerateSpriteCollection();
}