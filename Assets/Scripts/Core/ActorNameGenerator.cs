﻿using UnityEngine;
using System.IO;
using System.Xml;

public class ActorNameGenerator
{
    private string[] maleNamesArray;
    private string[] femaleNamesArray;

    public ActorNameGenerator()
    {
        var xmlRawFile = Resources.Load<TextAsset>("ActorNames");
        string xmlData = xmlRawFile.text;
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(new StringReader(xmlData));

        string[] seperatingChars = { ", " };
        XmlNode nodeMaleNames = xmlDocument.SelectSingleNode("/data/names/male");
        string maleNamesString = nodeMaleNames.InnerXml;
        maleNamesArray = maleNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);
        XmlNode nodeFemaleNames = xmlDocument.SelectSingleNode("/data/names/female");
        string femaleNamesString = nodeFemaleNames.InnerXml;
        femaleNamesArray = femaleNamesString.Split(seperatingChars, System.StringSplitOptions.RemoveEmptyEntries);
    }

    public string GeneratedName(ActorGenders actorGender)
    {
        if (actorGender == ActorGenders.Male)
        {
            int nameIndexMax = maleNamesArray.Length;
            int randomNameIndex = Random.Range(0, nameIndexMax);
            return maleNamesArray[randomNameIndex];
        }
        else if (actorGender == ActorGenders.Female)
        {
            int nameIndexMax = femaleNamesArray.Length;
            int randomNameIndex = Random.Range(0, nameIndexMax);
            return femaleNamesArray[randomNameIndex];
        }
        else
        {
            Debug.LogError("No names stored for ActorGenders.NONE");
            return null;
        }
    }
}
