﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteData : MonoBehaviour
{
    public List<Sprite> BodySprites;
    public List<Sprite> LegSprites;
    public List<Sprite> HairSprites;
}
