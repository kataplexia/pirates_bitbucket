﻿using UnityEngine;

public class PlayerActor : Actor
{
    private Data data;

    public EquipableItem equipmentArmor;
    public EquipableItem equipmentWeapon;
    public EquipableItem equipmentTrinket;
    public int ActorCost;

    public PlayerActor()
    {
        data = GameObject.Find("_DATA").GetComponent<Data>();

        ActorQuality = RandomizeQuality();
        ActorGender = RandomizeGender();
        ActorZodiac = RandomizeZodiac();
        ActorNature = RandomizeNature();
        Name = data.ActorNameGenerator.GeneratedName(ActorGender) + " the " + GenerateNameSuffix();
        GenerateSpriteCollection();

        GenerateBaseStatValues();
        ModifyBaseStatValues();

        ActorCost = 150;
    }

    private ActorQualities RandomizeQuality()
    {
        //TODO: Add roll weighting to quality assignment based on current progression
        int randomIndex = Random.Range(0, 4);
        switch (randomIndex)
        {
            case 0:
                QualityColor = Color.white;
                return ActorQualities.Common;

            case 1:
                QualityColor = Color.green;
                return ActorQualities.Uncommon;

            case 2:
                QualityColor = Color.blue;
                return ActorQualities.Rare;

            case 3:
                QualityColor = Color.magenta;
                return ActorQualities.Epic;
        }
        return ActorQualities.NONE;
    }

    private ActorGenders RandomizeGender()
    {
        int randomIndex = Random.Range(0, 2);
        switch (randomIndex)
        {
            case 0:
                return ActorGenders.Male;

            case 1:
                return ActorGenders.Female;
        }
        return ActorGenders.NONE;
    }

    private ActorZodiacs RandomizeZodiac()
    {
        int randomIndex = Random.Range(0, 12);
        switch (randomIndex)
        {
            case 0:
                ZodiacColor = Color.red;
                return ActorZodiacs.Aries;

            case 1:
                ZodiacColor = Color.magenta;
                return ActorZodiacs.Leo;

            case 2:
                ZodiacColor = Color.cyan;
                return ActorZodiacs.Cancer;

            case 3:
                ZodiacColor = Color.blue;
                return ActorZodiacs.Pisces;

            case 4:
                ZodiacColor = Color.cyan;
                return ActorZodiacs.Scorpio;

            case 5:
                ZodiacColor = Color.yellow;
                return ActorZodiacs.Taurus;
            
            case 6:
                ZodiacColor = Color.green;
                return ActorZodiacs.Sagitarius;

            case 7:
                ZodiacColor = Color.red;
                return ActorZodiacs.Gemini;

            case 8:
                ZodiacColor = Color.blue;
                return ActorZodiacs.Virgo;

            case 9:
                ZodiacColor = Color.green;
                return ActorZodiacs.Libra;

            case 10:
                ZodiacColor = Color.yellow;
                return ActorZodiacs.Capricorn;

            case 11:
                ZodiacColor = Color.magenta;
                return ActorZodiacs.Aquarius;
        }
        return ActorZodiacs.NONE;
    }

    private ActorNatures RandomizeNature()
    {
        int randomIndex = Random.Range(0, 3);
        switch (randomIndex)
        {
            case 0:
                return ActorNatures.Forceful;

            case 1:
                return ActorNatures.Cunning;

            case 2:
                return ActorNatures.Wise;
        }
        return ActorNatures.NONE;
    }

    private string GenerateNameSuffix()
    {
        switch (ActorNature)
        {
            case ActorNatures.Forceful:
                switch (ActorQuality)
                {
                    case ActorQualities.Common:
                        return "Brash";

                    case ActorQualities.Uncommon:
                        return "Bold";

                    case ActorQualities.Rare:
                        return "Brave";

                    case ActorQualities.Epic:
                        return "Titanic";
                }
                break;

            case ActorNatures.Cunning:
                switch (ActorQuality)
                {
                    case ActorQualities.Common:
                        return "Careful";

                    case ActorQualities.Uncommon:
                        return "Crafty";

                    case ActorQualities.Rare:
                        return "Cunning";

                    case ActorQualities.Epic:
                        return "Slick";
                }
                break;

            case ActorNatures.Wise:
                switch (ActorQuality)
                {
                    case ActorQualities.Common:
                        return "Windy";

                    case ActorQualities.Uncommon:
                        return "Smart";

                    case ActorQualities.Rare:
                        return "Wise";

                    case ActorQualities.Epic:
                        return "Magnificent";
                }
                break;
        }
        return null;
    }

    public override void GenerateSpriteCollection()
    {
        int bodySpriteRandomIndex = Random.Range(0, data.SpriteData.BodySprites.Count);
        BodySprite = data.SpriteData.BodySprites[bodySpriteRandomIndex];

        int legsSpriteRandomIndex = Random.Range(0, data.SpriteData.LegSprites.Count);
        LegsSprite = data.SpriteData.LegSprites[legsSpriteRandomIndex];

        int hairSpriteRandomIndex = Random.Range(0, data.SpriteData.HairSprites.Count);
        HairSprite = data.SpriteData.HairSprites[hairSpriteRandomIndex];
    }

    private void GenerateBaseStatValues()
    {
        int qualityModifier = 0;
        switch (ActorQuality)
        {
            case ActorQualities.Uncommon:
                qualityModifier = 4;
                break;

            case ActorQualities.Rare:
                qualityModifier = 7;
                break;

            case ActorQualities.Epic:
                qualityModifier = 9;
                break;
        }

        AttackPowerValue = Random.Range(5, 9) + qualityModifier;
        MagicPowerValue = Random.Range(5, 9) + qualityModifier;
        CriticalChanceValue = Random.Range(5, 9) + qualityModifier;
        PhysicalResistanceValue = Random.Range(5, 9) + qualityModifier;
        MagicResistanceValue = Random.Range(5, 9) + qualityModifier;
        DodgeChanceValue = Random.Range(5, 9) + qualityModifier;
        CurrentHealthValue = Random.Range(20, 26) + qualityModifier;
        MaxHealthValue = CurrentHealthValue;

        HitChanceValue = 90 + (int)(qualityModifier / 2);
        SpeedValue = 1;
    }

    private void ModifyBaseStatValues()
    {
        int attackPowerModifier = 0;
        int magicPowerModifier = 0;
        int criticalChanceModifier = 0;
        int physicalResistanceModifier = 0;
        int magicResistanceModifier = 0;
        int dodgeChanceModifier = 0;
        int healthModifier = 0;

        switch (ActorZodiac)
        {
            case ActorZodiacs.Aries:
                attackPowerModifier = (int)(Random.Range(5, 9) / 2);
                physicalResistanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = (int)(Random.Range(2, 5) * 2);
                break;

            case ActorZodiacs.Leo:
                attackPowerModifier = (int)(Random.Range(5, 9) / 2);
                magicResistanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = Random.Range(2, 5);
                break;

            case ActorZodiacs.Cancer:
                magicPowerModifier = (int)(Random.Range(5, 9) / 2);
                dodgeChanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = (int)(Random.Range(2, 5) / 2);
                break;

            case ActorZodiacs.Pisces:
                magicPowerModifier = (int)(Random.Range(5, 9) / 2);
                magicResistanceModifier = (int)(Random.Range(5, 9) / 2);
                break;

            case ActorZodiacs.Scorpio:
                criticalChanceModifier = (int)(Random.Range(5, 9) / 2);
                magicResistanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = (int)(Random.Range(2, 5) / 2);
                break;

            case ActorZodiacs.Taurus:
                attackPowerModifier = (int)(Random.Range(5, 9) / 2);
                dodgeChanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = (int)(Random.Range(2, 5) * 1.5f);
                break;

            case ActorZodiacs.Sagitarius:
                criticalChanceModifier = (int)(Random.Range(5, 9) / 2);
                dodgeChanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = Random.Range(2, 5);
                break;

            case ActorZodiacs.Gemini:
                attackPowerModifier = (int)(Random.Range(5, 9) / 2);
                physicalResistanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = (int)(Random.Range(2, 5) * 2);
                break;

            case ActorZodiacs.Virgo:
                magicPowerModifier = (int)(Random.Range(5, 9) / 2);
                magicResistanceModifier = (int)(Random.Range(5, 9) / 2);
                break;

            case ActorZodiacs.Libra:
                criticalChanceModifier = (int)(Random.Range(5, 9) / 2);
                dodgeChanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = Random.Range(2, 5);
                break;

            case ActorZodiacs.Capricorn:
                criticalChanceModifier = (int)(Random.Range(5, 9) / 2);
                physicalResistanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = (int)(Random.Range(2, 5) * 1.5f);
                break;

            case ActorZodiacs.Aquarius:
                magicPowerModifier = (int)(Random.Range(5, 9) / 2);
                physicalResistanceModifier = (int)(Random.Range(5, 9) / 2);
                healthModifier = Random.Range(2, 5);
                break;
        }

        switch (ActorNature)
        {
            case ActorNatures.Forceful:
                attackPowerModifier += (int)(Random.Range(5, 9) / 2);
                physicalResistanceModifier += (int)(Random.Range(5, 9) / 2);
                healthModifier += Random.Range(2, 5);
                SpeedValue = 2;
                break;

            case ActorNatures.Cunning:
                criticalChanceModifier += (int)(Random.Range(5, 9) / 2);
                dodgeChanceModifier += (int)(Random.Range(5, 9) / 2);
                healthModifier += (int)(Random.Range(2, 5) / 2);
                SpeedValue = 3;
                break;

            case ActorNatures.Wise:
                magicPowerModifier += (int)(Random.Range(5, 9) / 2);
                magicResistanceModifier += (int)(Random.Range(5, 9) / 2);
                SpeedValue = 1;
                break;
        }

        AttackPowerValue += attackPowerModifier;
        MagicPowerValue += magicPowerModifier;
        CriticalChanceValue += criticalChanceModifier;
        PhysicalResistanceValue += physicalResistanceModifier;
        MagicResistanceValue += magicResistanceModifier;
        DodgeChanceValue += dodgeChanceModifier;
        CurrentHealthValue += healthModifier;
        MaxHealthValue = CurrentHealthValue;
    }
}
